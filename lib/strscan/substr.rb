
class StringScanner
  
  # @param [Integer] start some value of {StringScanner#pos}.
  # @param [Integer] end_ some value of {StringScanner#pos}.
  # @return [String]
  def substr(start, end_)
    old_pos = self.pos
    self.pos = start
    result = peek(end_ - start)
    self.pos = old_pos
    return result
  end
  
end
  
