
# To disable YARD warnings:
# 
# @!parse
#   class Object
#   end

# 
# @example
#   
#   method_names = {
#     :eat => "M1",
#     :cut => "M2"
#   }
#   
#   c = code <<
#     "class Apple \n" <<
#     "{ \n" <<
#     "public: \n" <<
#     "    void " << non_code(:eat) << "(); \n" <<
#     "    void " << non_code(:cut) << "(int PiecesCount); \n" <<
#     "}; \n"
#   c.non_code_parts.each do |part|
#     if part.is_a? Symbol then
#       if not method_names.key? part then
#         raise "method name not found!"
#       end
#     end
#   end
#   c = c.map_non_code_parts do |part|
#     if part.is_a? Symbol then
#       method_names[part]
#     end
#   end
#   puts c
#     # class Apple 
#     # { 
#     # public: 
#     #     void M1(); 
#     #     void M2(int PiecesCount); 
#     # }; 
# 
class Code
  
  # @api private
  # @note used by {Code}, {::code}, {::non_code} only.
  # 
  # @param [Array<String, NonCodePart>] parts
  # @param [Object, nil] metadata
  # 
  def initialize(parts, metadata = nil)
    @parts = parts
    @metadata = metadata
  end
  
  # @overload + str
  #   @param [String] str
  #   @return [Code]
  # @overload + code
  #   @param [Code] code
  #   @return [Code]
  def + arg
    case arg
    when String then self + Code.new([arg])
    when Code then Code.new(self.parts + arg.parts)
    end
  end
  
  # @overload << str
  #   Appends +str+ to self.
  #   @param [String] str
  #   @return [self]
  # @overload << code
  #   Appends +str+ to self.
  #   @param [Code] code
  #   @return [self]
  def << arg
    case arg
    when String then self << Code.new([arg])
    when Code then @parts.concat(arg.parts); self
    end
  end
  
  # @overload metadata(obj)
  #   @param [Object] obj
  #   @return [Code] a {Code} with +obj+ attached to it. The +obj+ can later
  #     be retrieved with {#metadata}().
  # @overload metadata
  #   @return [Object, nil] an {Object} attached to this {Code} with
  #     {#metadata}(obj) or nil if no {Object} was attached.
  def metadata(*args)
    if args.empty?
    then @metadata
    else Code.new(@parts, args.first)
    end
  end
  
  # @yieldparam [Object] part
  # @yieldreturn [String]
  # @return [Code] a {Code} with {#non_code_parts} mapped by the passed block.
  def map_non_code_parts(&f)
    Code.new(
      @parts.map do |part|
        case part
        when String then part
        when NonCodePart then f.(part.data)
        end
      end
    )
  end
  
  # @return [Enumerable<Object>] non-code parts of this {Code} introduced
  #   with {::non_code}. See also {#map_non_code_parts}.
  def non_code_parts
    @parts.select { |part| part.is_a? NonCodePart }.map(&:data)
  end
  
  # @return [String]
  def to_s
    if (x = @parts.find { |part| part.is_a? NonCodePart }) then
      raise "non-code part: #{x.inspect}"
    end
    @parts.join
  end
  
  # @return [String]
  def inspect
    Inspectable.new(@parts, @metadata).inspect
  end
  
  # @api private
  # @note used by {Code}, {::non_code} only.
  NonCodePart = Struct.new :data
  
  protected
  
  # @!visibility private
  attr_reader :parts
  
  private
  
  # @!visibility private
  Inspectable = Struct.new :parts, :metadata
  
end

class Array
  
  # @param [Code, String] delimiter
  # @return [Code]
  def join_code(delimiter = "")
    self.reduce do |r, self_i|
      code << r << delimiter << self_i
    end or
    code
  end
  
end

# @overload code
#   @return [Code] an empty {Code}.
# @overload code(str)
#   @param [String] str
#   @return [Code] +str+ converted to {Code}.
def code(str = nil)
  if str
  then Code.new([str])
  else Code.new([])
  end
end

# @param [Object] data
# @return [Code] a {Code} consisting of the single non-code part +data+.
#   See also {Code#non_code_parts}.
def non_code(data)
  Code.new([Code::NonCodePart.new(data)])
end

alias __non_code__ non_code
