require 'strscan'
require 'strscan/substr'

# To disable YARD warnings:
# @!parse
#   class Exception
#     def message
#     end
#   end
#   class Array
#   end
#   class String
#   end
#   class StringScanner
#     attr_reader :pos
#   end

# 
class Parse
  
  # 
  # parses +text+.
  # 
  # @param [String] text
  # @param [String] file file the +text+ is taken from.
  # @raise [Parse::Error, IOError]
  # @return [Object] what {#start} returns (non-nil).
  # 
  def call(text, file = "-")
    @text = StringScanner.new(text)
    @file = file
    @most_probable_error = nil
    @allow_errors = true
    @rule_start_pos = nil
    r = start()
    if r.nil? or not @text.eos? then
      if @most_probable_error
      then raise @most_probable_error
      else raise Error.new(StringScannerPosition.new(@text.pos, @text, @file), "syntax error")
      end
    end
    return r
  end
  
  module Position
    
    include Comparable
    
    # @param [Integer] line
    # @param [Integer] column
    # @param [String] file
    # @return [Position]
    def self.new(line, column, file = "-")
      Position2.new(line, column, file)
    end
    
    # @!method file
    #   @abstract
    #   @return [String]
    
    # @!method line
    #   @abstract
    #   @return [Integer]
    
    # @!method column
    #   @abstract
    #   @return [Integer]
    
    # @return [-1, 0, 1, nil] nil if +other+.{#file} != self.{#file} or
    #   +other+ is not a {Position}. Otherwise it compares {#line} and {#column}
    #   and returns -1, 0 or 1. See also {Comparable#<=>}.
    def <=> other
      return nil unless other.is_a? Position
      return nil unless self.file == other.file
      x = self.line <=> other.line
      return x if x != 0
      return self.column <=> other.column
    end
    
    # @return [Boolean]
    def == other
      return false unless other.is_a? Position
      return (self <=> other) == 0
    end
    
    # @return [String]
    def to_s
      "#{file}:#{line}:#{column}"
    end
    
  end
  
  class Error < Exception
    
    # @param [Position] pos
    # @param [String] message
    def initialize(pos, message)
      super(message)
      @pos = pos
    end
    
    # @return [Position]
    attr_reader :pos
    
    # @param [Error] other +self+.{#pos} must be equal to +other+.{#pos}.
    # @return [Error] an {Error} with {Exception#message} combined from
    #   {Exception#message}s of +self+ and +other+ (using "or" word).
    def or other
      raise "#{self.pos} == #{other.pos} is false" unless self.pos == other.pos
      Error.new(pos, "#{self.message} or #{other.message}")
    end
    
  end
  
  class Expected < Error
    
    # @param [Position] pos
    # @param [String] what_1
    # @param [*String] what_2_n
    def initialize(pos, what_1, *what_2_n)
      @what = [what_1, *what_2_n]
      super(pos, "#{@what.join(", ")} expected")
    end
    
    # (see Error#or)
    def or other
      if other.is_a? Expected
        raise "#{self.pos} == #{other.pos} is false" unless self.pos == other.pos
        Expected.new(pos, *(self.what + other.what).uniq)
      else
        super(other)
      end
    end
    
    protected
    
    # @!visibility private
    # @return [Array<String>]
    attr_reader :what
    
  end
  
  protected
  
  # @!method start()
  #   @abstract
  #   
  #   Implementation of {#call}. Or the starting rule.
  #   
  #   @return [Object, nil] a result of parsing or nil if the parsing failed.
  #   @raise [Parse::Error] if the parsing failed.
  #   @raise [IOError]
  #   
  
  # 
  # scans +arg+ in +text+ passed to {#call} starting from {#pos} and,
  # if scanned successfully, advances {#pos} and returns the scanned
  # sub-{String}. Otherwise it calls {#expected} and returns nil.
  # 
  # @param [String, Regexp] arg
  # @return [String, nil]
  # 
  def scan(arg)
    case arg
    when Regexp then @text.scan(arg) or expected(pos, %(regexp "#{arg.source}"))
    when String then @text.scan(Regexp.new(Regexp.escape(arg))) or expected(pos, %("#{arg}"))
    end
  end
  
  # alias for {#_1} and {#_2}
  def _(arg = nil, &block)
    if arg
    then _1(arg)
    else _2(&block)
    end
  end
  
  # -------------
  # @!group Rules
  # -------------
  
  # defines method +name+ with body +body+. Inside +body+ {#rule_start_pos}
  # is the value of {#pos} right before the defined method's call.
  # 
  # @param [Symbol] name
  # @return [void]
  # 
  def self.rule(name, &body)
    define_method(name) do
      old_rule_start_pos = @rule_start_pos
      @rule_start_pos = pos
      begin
        if $DEBUG then STDERR.puts("#{pos.file}:#{pos.line+1}:#{pos.column+1}: entering rule :#{name}"); end
        r = instance_eval(&body)
        if $DEBUG then t = if r.nil? then "(with nil)" else "(with #{r.class.to_s})" end; STDERR.puts("#{pos.file}:#{pos.line+1}:#{pos.column+1}: exiting rule :#{name} #{t}"); end
        r
      ensure
        @rule_start_pos = old_rule_start_pos
      end
    end
  end
  
  # See {Parse::rule}.
  # 
  # @return [Position]
  # 
  attr_reader :rule_start_pos
  
  # @param [ASTNode] node
  # @return [ASTNode] +node+ which is
  #   {ASTNode#initialize_pos}({#rule_start_pos})-ed.
  def _1(node)
    node.initialize_pos(rule_start_pos)
  end
  
  # @overload token(name, [description], string | regexp | &body)
  # 
  # A shorthand for:
  #   
  #   rule name do
  #     expect(description) {
  #       no_errors {
  #         r = (scan(string) | scan(regexp) | body) and
  #         whitespace_and_comments and
  #         r
  #       }
  #     }
  #   end
  # 
  # Method +whitespace_and_comments+ must be defined!
  # 
  # +description+ defaults to <code>"`#{string}'"</code>,
  # <code>%(regexp "#{regexp}")</code> or <code>"#{name}"</code> (in that
  # order, depending on what is defined).
  # 
  # @return [void]
  def self.token(name, *args, &body)
    pattern, body =
      if body.nil? then
        pattern = args.pop
        [pattern, proc { scan(pattern) }]
      else
        [nil, body]
      end
    description =
      args.pop ||
      case pattern
      when nil then "#{name}"
      when String then "`#{pattern}'"
      when Regexp then %(regexp "#{pattern.source}")
      end
    raise ArgumentError, "wrong number of arguments" unless args.empty?
    rule name do
      expect(description) {
        no_errors {
          r = instance_eval(&body) and whitespace_and_comments and r
        }
      }
    end
  end
  
  # ----------
  # @!endgroup
  # ----------
  
  # ----------------
  # @!group Position
  # ----------------
  
  # @return [Position] current {Position} in +text+ passed to {#call}.
  def pos
    StringScannerPosition.new(@text.pos, @text, @file)
  end
  
  # is {#pos} at the end of the text?
  def end?
    @text.eos?
  end
  
  alias eos? end?
  
  # is {#pos} at the beginning of the text?
  def begin?
    @text.pos == 0
  end
  
  # ----------
  # @!endgroup
  # ----------
  
  # --------------------------
  # @!group Parser Combinators
  # --------------------------
  
  # calls +f+ and returns true.
  # 
  # @return [true]
  def act(&f)
    f.()
    true
  end
  
  #   
  # calls +f+. If +f+ results in nil then it restores {#pos} to the
  # value before the call.
  # 
  # @return what +f+ returns.
  # 
  def _2(&f)
    old_text_pos = @text.pos
    f.() or begin
      @text.pos = old_text_pos
      nil
    end
  end
  
  # calls +f+ using {#_2} many times until it returns nil.
  # 
  # @return [Array] an {Array} of non-nil results of +f+.
  # 
  def many(&f)
    r = []
    while true
      f0 = _(&f)
      if f0
      then r.push(f0)
      else break
      end
    end
    r
  end
  
  # calls +f+ using {#_2}.
  # 
  # @return [Array] an empty {Array} if +f+ results in nil and an {Array}
  #   containing the single result of +f+ otherwise.
  # 
  def opt(&f)
    [_(&f)].compact
  end
  
  # The same as <code>f.() and many(&f)</code>.
  # 
  # @return [Array, nil] an {Array} of results of +f+ or nil if the first call
  #   to +f+ returned nil.
  # 
  def one_or_more(&f)
    f1 = f.() and f2_n = many(&f) and [f1, *f2_n]
  end
  
  alias many1 one_or_more
  
  # @overload not_follows(*method_ids)
  #   
  #   calls methods specified by +method_ids+. If any of them returns non-nil
  #   then this method returns nil, otherwise it returns true. The methods
  #   are called inside {#no_errors}. {#pos} is restored after each method's
  #   call.
  #   
  #   @param [Array<Symbol>] method_ids
  #   @return [true, nil]
  #   
  # @overload not_follows(&f)
  # 
  #   calls +f+. If +f+ returns non-nil then this method returns nil,
  #   otherwise it returns true. +f+ is called inside {#no_errors}.
  #   {#pos} is restored after +f+'s call.
  #   
  #   @return [true, nil]
  # 
  def not_follows(*method_ids, &f)
    if f then
      if no_errors { _{ f.() } }
      then nil
      else true
      end
    else # if not method_ids.empty? then
      if no_errors { method_ids.any? { |method_id| _{ __send__(method_id) } } }
      then nil
      else true
      end
    end
  end
  
  # ----------
  # @!endgroup
  # ----------
  
  # --------------
  # @!group Errors
  # --------------
  
  # 
  # sets {#most_probable_error} to +error+ if it is more probable than
  # {#most_probable_error} or if {#most_probable_error} is nil.
  # 
  # @param [Error] error
  # @return [nil]
  # 
  def error(error)
    if @allow_errors then
      if @most_probable_error.nil? or @most_probable_error.pos < error.pos then
        @most_probable_error = error
      elsif @most_probable_error and @most_probable_error.pos == error.pos then
        @most_probable_error = @most_probable_error.or error
      else
        # do nothing
      end
    end
    return nil
  end
  
  # macro
  def expected(pos, what_1, *what_2_n)
    error(Expected.new(pos, what_1, *what_2_n))
  end
  
  # macro
  def expect(what_1, *what_2_n, &body)
    p = pos and body.() or expected(p, what_1, *what_2_n)
  end
  
  # calls +block+. Inside the +block+ {#error} has no effect.
  # 
  # @return what +block+ returns.
  # 
  def no_errors(&block)
    old_allow_errors = @allow_errors
    begin
      @allow_errors = false
      block.()
    ensure
      @allow_errors = old_allow_errors
    end
  end
  
  # @return [Error, nil]
  attr_reader :most_probable_error
  
  # ----------
  # @!endgroup
  # ----------
  
  private
  
  # @api private
  # @note used by {Position#new} only.
  class Position2
    
    include Position
    
    # @param [Integer] line
    # @param [Integer] column
    # @param [String] file
    def initialize(line, column, file)
      @line = line
      @column = column
      @file = file
    end
    
    # (see Position#file)
    attr_reader :file
    
    # (see Position#column)
    attr_reader :column
    
    # (see Position#line)
    attr_reader :line
    
  end
  
  # @!visibility private
  # 
  # A {Position} optimized for using with {StringScanner}.
  # 
  class StringScannerPosition
    
    include Position
    
    # @param [Integer] text_pos {StringScanner#pos} in +text+.
    # @param [StringScanner] text
    # @param [String] file see {Position#file}.
    def initialize(text_pos, text, file)
      @text = text
      @text_pos = text_pos
      @file = file
    end
    
    # (see Position#file)
    attr_reader :file
    
    # (see Position#line)
    def line
      line_and_column[0]
    end
    
    # (see Position#column)
    def column
      line_and_column[1]
    end
    
    # (see Position#<=>)
    def <=> other
      case other
      when StringScannerPosition
        return nil unless self.file == other.file
        return self.text_pos <=> other.text_pos
      else
        super(other)
      end
    end
    
    # @!visibility private
    attr_reader :text_pos
    
    # @!visibility private
    attr_reader :text
    
    private
    
    def line_and_column
      @line_and_column ||= begin
        s = @text.substr(0, @text_pos)
        lines = s.split("\n", -1).to_a
        [
          line = if lines.size == 0 then 0 else lines.size - 1 end,
          column = (lines.last || "").size
        ]
      end
    end
    
  end
  
end

module ASTNode
  
  # macro
  def self.new(*properties, &body)
    (if properties.empty? then Class.new else Struct.new(*properties); end).tap do |c|
      c.class_eval do
        
        include ASTNode
        
        # @return [Boolean]
        def === other
          self.class == other.class and
          self.members.all? { |member| self.__send__(member) === other.__send__(member) }
        end
        
        if properties.empty? then
          # @return [Array<Symbol>]
          def members
            []
          end
        end
        
      end
      c.class_eval(&body) if body
    end
  end
  
  # 
  # Sets {#pos} to +pos+.
  # 
  # @param [Parse::Position] pos
  # @return [self]
  def initialize_pos(pos)
    @pos = pos
    self
  end
  
  # @note {#initialize_pos} must be called before this method can be used.
  # @return [Parse::Position]
  def pos
    raise "initialize_pos() must be called before this method can be used" unless @pos
    @pos
  end
  
end

# Alias for {ASTNode.new}.
def node(*properties, &body)
  ASTNode.new(*properties, &body)
end

# class Calc < Parse
#   
#   Number = ASTNode.new :val
#   
#   rule :start do
#     x1 = number and x2 = many { comma and number } and [x1, *x2]
#   end
#   
#   rule :number do
#     s = scan(/\d+/) and _(Number[s])
#   end
#   
#   token :comma, ","
#   
#   def whitespace_and_comments
#     scan(/\s+/)
#   end
#   
# end
# 
# begin
#   p Calc.new.("10a 20", __FILE__)
# rescue Parse::Error => e
#   puts "error: #{e.pos.file}:#{e.pos.line}:#{e.pos.column}: #{e.message}"
# end
