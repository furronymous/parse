
Gem::Specification.new do |s|
  s.name        = 'parse-framework'
  s.version     = '0.0.4'
  s.required_ruby_version = ">= 1.9.3"
  s.date        = '2017-05-28'
  s.summary     = "Parser building framework"
  s.description = <<-TEXT
The framework for building parsers. It features position tracking and automatic
error handling!
  TEXT
  s.homepage    = "http://furronymous.bitbucket.org/parse"
  s.authors     = ["Various Furriness"]
  s.email       = 'various.furriness@gmail.com'
  s.files       = Dir["lib/**/*.rb"] + ["README.md", ".yardopts"]
  s.license     = 'MIT'
end
